﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public float speed;
    public float boarderThickness;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Movement();

        Vector3 position = transform.position;

        //move up
        if (Input.mousePosition.y >= Screen.height - boarderThickness)
        {
            position.z += speed * Time.deltaTime;
        }
        //move down
        if (Input.mousePosition.y <= boarderThickness)
        {
            position.z -= speed * Time.deltaTime;
        }
        //move left
        if (Input.mousePosition.x >= Screen.width -boarderThickness)
        {
            position.x += speed * Time.deltaTime;
        }
        //move right
        if (Input.mousePosition.x <= boarderThickness)
        {
            position.x -= speed * Time.deltaTime;
        }

        transform.position = position;
    }

    void Movement()
    {
        Vector3 position = transform.position;

        //move up
        if (Input.mousePosition.y >= Screen.height - boarderThickness)
        {
            position.z += speed * Time.deltaTime;
        }
        //move down
        if (Input.mousePosition.y >= Screen.height - boarderThickness)
        {
            position.z -= speed * Time.deltaTime;
        }
        //move left
        if (Input.mousePosition.y >= Screen.height - boarderThickness)
        {
            position.x -= speed * Time.deltaTime;
        }
        //move right
        if (Input.mousePosition.y >= Screen.height - boarderThickness)
        {
            position.x += speed * Time.deltaTime;
        }

        transform.position = position;
    }
}
